const express = require('express');
const mongodb = require('mongodb');


const router = express.Router();

// Get Posts 
router.get('/', async (req, res) => {
    try {

        const  posts = await loadPostsCollection();
        res.send(await posts.find({}).toArray());

    } catch (error) {
        console.log('client error');
        throw error;
    }
    
});

// Add Post

router.post('/', async (req, res) => {
    try{
        const posts = await loadPostsCollection();
        await posts.insertOne({
            text: req.body.text,
            createdate: new Date()
        }).catch(e => {
                console.log('Insert did not work.')
                throw e
                return;
                });
    }catch(error) {
        console.log('client error');
        throw error;
    }
    res.status(201).send();
});


// Delete Post
router.delete('/:id', async (req,res) => {
    try {
        const posts = await loadPostsCollection();
        await posts.deleteOne
            ({ _id: new mongodb.ObjectID(req.params.id)}).catch(e => {
                console.log('Del did not work.')
                throw e
                return;
                });
    }catch( error ) {
        console.log('client error');
        throw error;
        return;
    }
    res.status(200).send();
});


//  function
async function loadPostsCollection() {
    try{
        const client = await mongodb.MongoClient.connect
        ('mongodb://dbadmin:sup123@ds133388.mlab.com:33388/tim-mgdb', {
        useNewUrlParser: true
        });
        return client.db('tim-mgdb').collection('posts');
    } catch (error){
        console.log('that didnt go well');
        throw error;
    }
    
    
}


module.exports = router;